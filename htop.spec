Name:		htop
Version:	3.3.0
Release:	2
Summary:	htop - an interactive process viewer
License:	GPLv2
URL:		https://htop.dev
Source0:	https://github.com/htop-dev/htop/archive/%{version}.tar.gz#/%{name}-%{version}.tar.gz

BuildRequires: perl perl-PathTools ncurses-devel autoconf automake gcc

Patch0001:     0001-Add-HeaderLayout-format-for-one-single-100-width-column.patch
Patch0002:     0002-Update-__STDC_VERSION__-check-with-C23.patch

%description
htop is a cross-platform interactive process viewer.
htop allows scrolling the list of processes vertically and horizontally to see their full command lines and related information like memory and CPU consumption.
The information displayed is configurable through a graphical setup and can be sorted and filtered interactively.
Tasks related to processes (e.g. killing and renicing) can be done without entering their PIDs.

%prep
%autosetup -n %{name}-%{version} -p1

%build
./autogen.sh
%configure
%make_build

%install
%make_install

%files
%license COPYING
%{_bindir}/%{name}
%{_datadir}/applications/%{name}.desktop
%{_datadir}/icons/hicolor/scalable/apps/htop.svg
%{_datadir}/pixmaps/%{name}.png
%{_mandir}/man1/%{name}.1.gz

%changelog
* Fri Jul 5 2024 zhangxingrong-<zhangxingrong@uniontech.cn> - 3.3.0-2
- Round Upstream Patchs
  - Add HeaderLayout format for one single 100%-width column
  - Update '__STDC_VERSION__' check with C23

* Thu Jan 25 2024 yixiangzhike <yixiangzhike007@163.com> - 3.3.0-1
- Update version to 3.3.0
  - htop add '-n/--max-itrations' option
  - implement zswap support
  - add IRQ PSI meter
  - refactor /proc/<pid>/status parsing
  - add option to shadow path prefixes
  - fix linux Platform_getProcessLocks() for file-description locks
  - fix format specifier warning on 32 bit
  - prevent null pointer dereference on ealy error

* Sun Oct 9 2022 ckji <jichengke2011@gmail.com> - 3.2.1-1
- Update version to 3.2.1

* Mon Apr 25 2022 wangkai <wangkai@h-partners.com> - 3.1.2-1
- Update version to 3.1.2

* Tue Jul 8 2021 xuguangmin <xuguangmin@kylinos.cn> - 3.0.5-3
- Update version to 3.0.5

* Mon Jun 7 2021 baizhonggui <baizhonggui@huawei.com> - 3.0.2-2
- Fix building error: ./autogen.sh: line 3: autoreconf: command not found
- Add autoconf/automake/gcc in BuildRequires

* Thu Sep 24 2020 Zhipeng Xie <xiezhipeng1@huawei.com> - 3.0.2-1
- Package init
